/*****************************************************************************

NAME
    mstrans.c -- translate MS-DOS and Wordstar text files into pure ASCII

SYNOPSIS
    mstrans [-vwcr] [file...]

DESCRIPTION
    This program may be used to filter MS-DOS text files for UNIX and/or
ASCII-ize WordStar document files.
    MS-DOS text files use an <CR><LF> terminator rather than UNIX's <LF>.
They end with control-Z characters (hex 1A) filling out their last 128
bytes. This filter squeezes out control-Z characters. The UNIX version of
this program strips CRs by default; the MS-DOS version doesn't. The -c
inverts the default.
    WordStar text files use the meta bits of characters as formatting
information; the -w option strips those off. It also turns WordStar hyphens
into ASCII hyphens and discards the invisible hyphen-break marks left when
hyphenation changes. These WordStar translations should be harmless to
ordinary text and program files.
    If called with no arguments, this tool will filter standard input to
standard output. If given arguments, it assumes they are all-caps MS-DOS
filenames and translates each into a UNIX equivalent in all small letters.
It will not translate a file with a name that is already all smalls, and
the UNIX version will not attempt to translate directory files. Thus, invoking

    mstrans *

in a directory containing MS-DOS files with all-caps names will do the
right thing.
   The -r option reverses these transformations, turning <LF> into <CR><LF>.

HISTORY
    Eric S. Raymond created 18 Oct 85

****************************************************************************/

#include <ctype.h> /* for tolower(), toupper() */
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* for strcmp() */
#include <unistd.h> /* for getopt() */
#include <sys/stat.h>
#include <sys/types.h>
#include <limits.h>

/* characters to treat specially */
#define LF '\012'    /* ASCII line feed */
#define CR '\015'    /* ASCII carriage return */
#define CTRLZ '\032' /* MS-DOS uses this to end text files */
#define SOFTH '\037' /* WordStar visible soft hyphen */
#define HMARK '\036' /* WordStar invisible hyphen break mark */

/* option flags */
bool verbose = false;  /* be chatty about skipped files */
bool wordstar = false; /* do WordStar translations */
bool reverse = false;  /* reverse translation, UNIX to MS-DOS */
bool stripcr = true; /* by default, strip carriage returns on UNIX */

void mstrans(void) {
	register int c;

	while ((c = getchar()) != EOF) {
		/* do to-MSDOS translations if enabled */
		if (reverse && c == LF) {
			putchar(CR);
			putchar(LF);
			continue;
		}

		/* strip out MS-DOS end-of-file marks */
		else if (c == CTRLZ || (c == CR && stripcr)) {
			continue;
		}

		/* do WordStar translations if enabled */
		if (wordstar) {
			c &= 0x7F; /* strip off WordStar meta bits */
			if (c == SOFTH) {
				c = '-'; /* change soft hyphens to real ones */
			} else if (c == HMARK) {
				continue; /* discard hyphen break marks */
			}
		}

		putchar(c);
	}
}

int main(int argc, char *argv[])
/* process filename arguments */
{
	char c, dst[PATH_MAX];
	int i;
	extern int optind;
	struct stat fs;

	while ((c = getopt(argc, argv, "vcwr")) != EOF) {
		if (c == 'v') {
			verbose = true;
		} else if (c == 'w') {
			wordstar = true;
		} else if (c == 'r') {
			reverse = true;
		} else if (c == 'c') {
			stripcr = !stripcr;
		}
	}

	if (optind == argc) {
		mstrans();
	} else {
		for (; optind < argc; optind++) {
			/* if the name is of anything but a file, skip it */
			if (stat(argv[optind], &fs) ||
			    (fs.st_mode & S_IFMT) != S_IFREG) {
				if (verbose) {
					fprintf(stderr,
					        "mstrans: %s is a directory or "
					        "special file\n",
					        argv[optind]);
				}
				continue;
			}

			/* generate an all-smalls version of the all-caps MS-DOS
			 * name */
			for (i = 0; i < sizeof(dst) - 1 &&
			            (c = argv[optind][i]) != '\0';
			     i++) {
				if (reverse) {
					dst[i] = toupper(c);
				} else {
					dst[i] = tolower(c);
				}
			}
			dst[i] = '\0';

			/* if the name is already all-smalls, forget it */
			if (strcmp(argv[optind], dst) == 0) {
				if (verbose) {
					fprintf(stderr,
					        "mstrans: skipping %s\n",
					        argv[optind]);
				}
				continue;
			}

			/* if we can open both source and destination file,
			 * translate */
			if (freopen(argv[optind], "r", stdin) &&
			    freopen(dst, "w", stdout)) {
				if (verbose) {
					fprintf(
					    stderr,
					    "mstrans: translating %s to %s\n",
					    argv[optind], dst);
				}
				mstrans();
			}
		}
	}
	return EXIT_SUCCESS;
}

/* mstrans.c ends here */
