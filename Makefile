# Makefile for the mstrans tool by Eric S. Raymond

VERS=$(shell sed -n <NEWS '/^[0-9]/s/:.*//p' | head -1)

CFLAGS=-O
LDFLAGS=-s

mstrans: mstrans.c

# Note: to suppress the footers with timestamps being generated in HTML,
# we use "-a nofooter".
# To debug asciidoc problems, you may need to run "xmllint --nonet --noout --valid"
# on the intermediate XML that throws an error.
.SUFFIXES: .html .adoc .1

.adoc.1:
	asciidoctor -D. -a nofooter -b manpage $<
.adoc.html:
	asciidoctor -D. -a nofooter -a webfonts! $<

install: mstrans.1 uninstall
	cp mstrans /usr/bin
	cp mstrans.1 /usr/share/man/man1/mstrans.1

uninstall:
	rm -f /usr/bin/mstrans /usr/share/man/man1/mstrans.1

clean:
	rm -f mstrans mstrans.o *~ mstrans.tar.gz
	rm -f *.rpm mstrans-*.tar.gz mstrans.1 *.html MANIFEST

CPPCHECKOPTS =
cppcheck:
	@cppcheck --quiet $(CPPCHECKOPTS) mstrans.c

reflow:
	@clang-format --style="{IndentWidth: 8, UseTab: ForIndentation}" -i $$(find . -name "*.[ch]")

SOURCES = READ.ME COPYING NEWS control mstrans.adoc Makefile mstrans.c

mstrans-$(VERS).tar.gz: $(SOURCES) mstrans.1
	@ls $(SOURCES) mstrans.1 | sed s:^:mstrans-$(VERS)/: >MANIFEST
	@(cd ..; ln -s mstrans mstrans-$(VERS))
	(cd ..; tar -czf mstrans/mstrans-$(VERS).tar.gz `cat mstrans/MANIFEST`)
	@(cd ..; rm mstrans-$(VERS))

dist: mstrans-$(VERS).tar.gz

release: mstrans-$(VERS).tar.gz mstrans.html
	shipper version=$(VERS) | sh -e -x

refresh: mstrans.html
	shipper -N -w version=$(VERS) | sh -e -x
