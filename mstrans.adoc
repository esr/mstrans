= mstrabs(1) =
:doctype: manpage

== NAME ==
mstrans - translate MS-DOS and Wordstar text files into pure ASCII

== SYNOPSIS ==
*mstranbs* [-v] [-w] [-c] [-r] [file...]

[[description]]
== DESCRIPTION ==
This program may be used to filter MS-DOS text files for UNIX and/or
ASCII-ize WordStar document files.

MS-DOS text files use an <CR><LF> terminator rather than UNIX's <LF>.
They end with control-Z characters (hex 1A) filling out their last 128
bytes. This filter squeezes out control-Z characters. The UNIX version
of this program strips CRs by default; the MS-DOS version doesn't. The
-c inverts the default.

WordStar text files use the meta bits of characters as
formatting information; the -w option strips those off. It also turns
WordStar hyphens into ASCII hyphens and discards the invisible
hyphen-break marks left when hyphenation changes. These WordStar
translations should be harmless to ordinary text and program
files.

If called with no arguments, this tool will filter standard input to
standard output. If given arguments, it assumes they are all-caps MS-DOS
filenames and translates each into a UNIX equivalent in all small letters.
It will not translate a file with a name that is already all smalls, and
the UNIX version will not attempt to translate directory files. Thus, invoking

----
+mstrans+ *
----

in a directory containing MS-DOS files with all-caps names will do the
right thing.

The -r option reverses these transformations, turning <LF> into <LF><CR>.

[[author]]
== AUTHOR ==
Eric S. Raymond <esr@snark.thyrsus.com>.  Surf to
http://www.catb.org/~esr
for updates and related resources.

// end
